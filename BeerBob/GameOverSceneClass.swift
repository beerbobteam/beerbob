//
//  GameOverSceneClass.swift
//  BeerBob
//
//  Created by Anderson Pagtabunan on 31/05/2017.
//  Copyright © 2017 Anderson Pagtabunan. All rights reserved.
//

import SpriteKit

class GameOverSceneClass: SKScene {
   
//   var parentScene: SKScene? = nil
   
   private var leaderboardsTableView = LeaderboardsTableView()
   private var shouldImplementLeaderboards: Bool = false
   private var mainMenuLabelNode: SKLabelNode!
   private var FBShareNode: SKNode!
   private var scoreLabelNode: SKLabelNode!
   private var highscoreLabelNode: SKLabelNode!
   private var restartNode: SKSpriteNode!
   private var allowInput: Bool = false
   
   override func didMove(to view: SKView) {
      //intialization
      initializeScene()
      initializeData()
      Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(changeAllowInput), userInfo: nil, repeats: false)
      
      // Table setup
      if shouldImplementLeaderboards {
         leaderboardsTableView.frame = CGRect(x:23.5,y:290,width:273,height:228)
         self.scene?.view?.addSubview(leaderboardsTableView)
         leaderboardsTableView.reloadData()
         
      } else {
         
         mainMenuLabelNode.run(.moveTo(y: 115, duration: 0))
//         FBShareNode.run(.moveTo(y: 80, duration: 0))
         let node = self.childNode(withName: "leaderboardBackgroundLabelNode")
         node?.removeFromParent()
         let node1 = self.childNode(withName: "leaderboardBackgroundNode")
         node1?.removeFromParent()
      }
   }
   
   @objc private func changeAllowInput() {
      allowInput = !allowInput
   }
   
   override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
      if allowInput {
         let touch = touches.first!
         let touchLocation = touch.location(in: self)
         if mainMenuLabelNode.contains(touchLocation) {
            leaderboardsTableView.removeFromSuperview()
            if let scene = BaseSceneClass(fileNamed: "BaseScene") {
               let reveal = SKTransition.fade(withDuration: 0.2)
               scene.scaleMode = .aspectFill
               self.view?.presentScene(scene, transition:reveal)
            }
         }
         if restartNode.contains(touchLocation) {
            print("restart touched")
            if let scene = BaseSceneClass(fileNamed: "BaseScene") {
               let reveal = SKTransition.fade(withDuration: 0.2)
               scene.scaleMode = .aspectFill
               scene.isRestarted = true
               self.view?.presentScene(scene, transition:reveal)
            }
         }
      }
//      if FBShareNode.contains(touchLocation) {
//         print("FBShareNode touched")
//      }
   }
   
   private func initializeData() {
      scoreLabelNode.text = String(gameObject.score)
      highscoreLabelNode.text = String(describing: currentUserData!.highscore)
      scoreLabelNode.run(.sequence([ .scale(to: 3, duration: 0.4), .scale(to: 1, duration: 0.3) ]))
   }
   
   private func initializeScene() {
      self.mainMenuLabelNode = self.childNode(withName: "MainMenuLabelNode") as? SKLabelNode
      self.scoreLabelNode = self.childNode(withName: "scoreLabelNode") as? SKLabelNode
      self.highscoreLabelNode = self.childNode(withName: "highscoreLabelNode") as? SKLabelNode
      self.restartNode = self.childNode(withName: "restartNode") as? SKSpriteNode
//      self.FBShareNode = self.childNode(withName: "FBShareNode")
      
      restartNode.run(.repeatForever(.sequence([.fadeAlpha(to: 0.3, duration: 0.4), .fadeIn(withDuration: 0.4)])))
      restartNode.run(.repeatForever(.sequence([ .scale(to: 0.3, duration: 0.4), .scale(to: 0.4, duration: 0.4) ])))
//      FBShareNode.run(.repeatForever(.sequence([.fadeAlpha(to: 0.3, duration: 0.5), .fadeIn(withDuration: 0.5)])))
   }
}

////gradient setup

//let gradient = CAGradientLayer()

//gradient.frame = CGRect(x: 0, y: 0, width: leaderboardsTableView.frame.width, height: leaderboardsTableView.frame.height)
//gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor, UIColor.clear.cgColor]
//gradient.locations = [0.0, 0.15, 0.25, 0.75, 0.85, 1.0]
//leaderboardsTableView.layer.mask = gradient
//leaderboardsTableView.backgroundColor = UIColor.clear








