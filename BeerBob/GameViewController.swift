//
//  GameViewController.swift
//  BeerBob
//
//  Created by Anderson Pagtabunan on 26/04/2017.
//  Copyright © 2017 Anderson Pagtabunan. All rights reserved.
//

import UIKit
import SpriteKit
import AVFoundation
//import GameplayKit

var backgroundMusic: AVAudioPlayer?
var slideSFX: AVAudioPlayer?
var musicOn: Bool = true
var effectsOn: Bool = true

class GameViewController: UIViewController, AVAudioPlayerDelegate {
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      if let view = self.view as! SKView? {
         // Load the SKScene from 'GameScene.sks'
               if let scene = BaseSceneClass(fileNamed: "BaseScene") {
//         if let scene = GameOverSceneClass(fileNamed: "GameOverScene") {
         
            // Set the scale mode to scale to fit the window
            scene.scaleMode = .aspectFill
            //        scene.scaleMode = .fill
            
            // Present the scene
            view.presentScene(scene)
         }
         
         view.ignoresSiblingOrder = true
         //      view.showsPhysics = true
         
//         view.showsFPS = true
//         view.showsDrawCount = true
//         view.showsNodeCount = true
      }
   }
   
//   private func loadSoundFiles() {
//      loadFXFile(ofName: "slide", soundFileExtension: "mp3", forSoundType: .slideFX)
//   }
   
   static func playSoundBG(_ play: Bool) {
//      Blind Love Dub
      if play {
         do {
            if let soundFileURL = Bundle.main.url(forResource: "bg", withExtension: "mp3") {
               let player: AVAudioPlayer? = try AVAudioPlayer(contentsOf: soundFileURL)
               if player != nil {
                  backgroundMusic = player
                  if backgroundMusic?.prepareToPlay() == true {
                     backgroundMusic?.play()
                     backgroundMusic?.numberOfLoops = -1
                     backgroundMusic?.volume = 0.8
                  }
               }
            } else {
               print("bg.mp3 not found on bundle.")
            }
            
         } catch let error {
            print("Failed to load the sound: \(error)")
         }
      } else {
         if backgroundMusic != nil {
            backgroundMusic?.stop()
            print("bg stopped.")
         }
      }
   }
   
   enum SoundType {
      case slideFX
   }
   static func loadFXFile(ofName soundFileName: String, soundFileExtension: String, forSoundType type: SoundType) {
      do {
         if let soundFileURL = Bundle.main.url(forResource: soundFileName, withExtension: soundFileExtension) {
            let player: AVAudioPlayer? = try AVAudioPlayer(contentsOf: soundFileURL)
            if player != nil {
               switch(type) {
               case .slideFX:
                  slideSFX = player
                  if slideSFX?.prepareToPlay() == true {
                     slideSFX?.play()
                  }
               }
            }
         } else {
            print("\"\(soundFileName).\(soundFileExtension)\" not found on bundle.")
         }
         
      } catch let error {
         print("Failed to load the sound: \(error)")
      }
   }
   
//   func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
//      if player == backgroundMusic {
//         backgroundMusic = nil
//         if backgroundMusic == nil {
//            print("nilled!")
//         }
//      }
//   }
   
   override var shouldAutorotate: Bool {
      return true
   }
   
   override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
      if UIDevice.current.userInterfaceIdiom == .phone {
         return .portrait
      } else {
         return .portrait
      }
   }
   
   override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      // Release any cached data, images, etc that aren't in use.
   }
   
   override var prefersStatusBarHidden: Bool {
      return true
   }
   
}



















