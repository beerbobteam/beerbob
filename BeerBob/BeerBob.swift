//
//  BeerBob.swift
//  BeerBob
//
//  Created by Anderson Pagtabunan on 02/05/2017.
//  Copyright © 2017 Anderson Pagtabunan. All rights reserved.
//

import Foundation
import CoreGraphics

enum Bob {
   enum Head: CGFloat {
      case Default = -0.336080600764028 //-19.256
      case Down = -0.601684806332525 //-34.474
      case Up = 0.138788582118589 //7.952
   }
   
   enum HeadBlendFactor: CGFloat {
      case Default = 0
      case Tipsy = 0.2
   }
   
   enum Arm: CGFloat {
      case Default = -1.28562698031154 //-73.6609999999998
      case Raised = 0.0394269878025519 //2.259
      case PartialRaised = -0.46474627322105 //-26.628
   }
   
   enum Mug: CGFloat {
      case Default = 1.3241813034881 //75.8700000000001
   }
}

enum MeterPercentage {
   enum Height: CGFloat {
      case Min = 0.0 //maximum
      case Max = 495//494.795 //minimum
   }
}

enum MeterVibrancy {
   enum Height: CGFloat {
      case Min = 6.138
      case Max = 501.3
   }
}

// Scene frameRect = 320 x 568

enum BeerPosition {
   enum OnHand: CGFloat {
      case X = 130
      case Y = 85
   }
   
   enum OutOfScreen: CGFloat {
      case X = 232.57
      case Y = 448.125
   }
}
// x
