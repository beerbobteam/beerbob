//
//  BaseSceneClass.swift
//  BeerBob
//
//  Created by Anderson Pagtabunan on 26/04/2017.
//  Copyright © 2017 Anderson Pagtabunan. All rights reserved.
//
import UIKit
import SpriteKit
import AVFoundation
//import GameplayKit

//extension Notification.Name {
//   static let drunkPercentageChanged = Notification.Name("drunkPercentageChanged")
//}

class NewGame {
   var score: Int = 0
   var prevScore: Int = 0
   var drunkPercentage: Double = 0.0
   var highScore: Int!
   
   var addedPercentage: Double = 30.0
   var deductedPercentage: Double = 1
   var screenshot: UIImage?
}

struct CurrentUserData {
   var highscore: Int
   
   init(highscore: Int) {
      self.highscore = highscore
   }
}

extension Notification.Name {
   static let didEnterBackground = Notification.Name("didEnterBackground")
}
extension AVPlayer {
   
   var isPlaying: Bool {
      return ((rate != 0) && (error == nil))
   }
}

class BaseSceneClass: SKScene, AVAudioPlayerDelegate {
   
   var isRestarted: Bool = false
   //  MARK: - VARIABLES
   private var headNode: SKSpriteNode!
   private var armNode: SKSpriteNode!
   private var mugNode: SKSpriteNode!
   private var cameraNode: SKCameraNode!
   private var logoNode: SKSpriteNode!
   private var meterNode: SKSpriteNode!
//   private var meterVibrancyNode: SKSpriteNode!
   private var meterPercentageNode: SKSpriteNode!
   private var settingsNode: SKSpriteNode!
   private var pauseNode: SKSpriteNode!
   private var pauseLabelNode: SKLabelNode!
   private var dimNode: SKSpriteNode!
   private var exitLabelNode: SKLabelNode!
   private var exitNode: SKSpriteNode!
   private var pauseIconNode: SKSpriteNode!
   
   private var tapToPlayLabelNode: SKLabelNode!
   private var notifyToTapNode: SKLabelNode!
   
   // in-game
   private var mugsNode: SKSpriteNode!
   private var scoreLabelNode: SKLabelNode!
   private var scoreBackgroundNode: SKSpriteNode!
   private var beerGushEmmiter: SKEmitterNode!
   
   // settings
   private var musicCheckboxNode: SKSpriteNode!
   private var effectsCheckboxNode: SKSpriteNode!
   private var musicCheckNode: SKSpriteNode!
   private var effectsCheckNode: SKSpriteNode!
   private var musicLabelNode: SKLabelNode!
   private var effectsLabelNode: SKLabelNode!
   private var creditsNode: SKNode!
   
   private var inGame: Bool = false
   
   private var tempPause: Bool = false
   private var pausedGame: Bool = false
   private var animatedNodes: [SKSpriteNode] = []
   private var taps: Int = 0
   private var timer = Timer()
   private var timerEmitter: Timer?
   private var timerEaseDifficulty: Timer?
   private var timerEaseDifficultyToInvalidate: TimeInterval?
   private var ingameTimerToTap = Timer()
   private var allowInput: Bool = false {
      didSet {
         print("allowInput = \"\(allowInput)\"")
      }
   }
   
//   override var isPaused: Bool {
//      get
//      {
//         return pausedGame
//      }
//      set
//      {
//         //we do not want to use newValue because it is being set without our knowledge
//         tempPause = newValue
//      }
//   }
   
//   private var creditsTextField: UILabel = {
//      
//      let label = UILabel()
//      label.backgroundColor = UIColor.init(white: 1, alpha: 0.1)
//      label.textColor = UIColor.white
//      label.numberOfLines = 0
////      label.font = UIFont.boldSystemFont(ofSize: 18)
//      label.font = UIFont(name: "SFDisplay-Light", size: 18)
//      label.textAlignment = .center
//      label.text = "Created by HardRocket Team\n\nBlind Love Dub by Jeris (c) copyright 2017 Licensed"
//      return label
//      
//   }()
   
   private var onSettings: Bool = false {
      didSet {
         print("onSettings = \"\(onSettings)\"")
      }
   }
   
   //SKActions
   let floatUp = SKAction.moveBy(x: 0.0, y: 100, duration: 1.5)
   let fadeOut = SKAction.fadeOut(withDuration: 1.5)
   let floatLeftAndRight = SKAction.repeatForever(.sequence([SKAction.moveBy(x: -10, y: 0, duration: 0.3), SKAction.moveBy(x: 20, y: 0, duration: 0.3), SKAction.moveBy(x: -10, y: 0, duration: 0.3)]))
   let scaleUp = SKAction.scale(by: 1.20, duration: 1.5)
   let removeNode = SKAction.sequence([.wait(forDuration: 1.5),.removeFromParent()])
   
   
   //  MARK: - SKCENE FUNCTIONS
   override func didMove(to view: SKView) {
      NotificationCenter.default.addObserver(self, selector: #selector(pauseBackPausedNodes), name: Notification.Name.didEnterBackground, object: nil)
      initializeGameComponents()
      getKeyChainData()
      getUserDefaultsData()
      
      if isRestarted {
         startGame()
      } else {
         startAnimation(phase: .Entry)
      }
   }
   
   @objc private func pauseBackPausedNodes() {
      print("received notification")
      if inGame && pausedGame {
//         pauseAllAnimatedNodes()
         didPauseGame(withPauseStatus: true)
      }
   }
   
   private func getUserDefaultsData() {
      let defaults = UserDefaults.standard
      
      if defaults.hasValue(forKey: "toggleMusic") {
         let retrieved = defaults.bool(forKey: "toggleMusic")
         musicOn = retrieved
         print("Has value.\n\t\"toggleMusic\" is \(retrieved)")
      } else {
         defaults.set(true, forKey: "toggleMusic")
         print("Has no value.\n\t\"toggleMusic\" is now \(true)")
      }
      
      if defaults.hasValue(forKey: "toggleFX") {
         let retrieved = defaults.bool(forKey: "toggleFX")
         effectsOn = retrieved
         print("Has value.\n\t\"toggleFX\" is \(retrieved)")
      } else {
         defaults.set(true, forKey: "toggleFX")
         print("Has no value.\n\t\"toggleFX\" is now \(true)")
      }
      
      defaults.synchronize()
      
      if musicOn {
         if backgroundMusic != nil {
            if backgroundMusic?.isPlaying == false {
               GameViewController.playSoundBG(true)
            }
         } else {
            GameViewController.playSoundBG(true)
         }
      }
   }
   
//   @objc private func addCreditsTextField() {
//      creditsTextField.frame = CGRect(x:37.7,y:327.737,width:244.05,height:128.787)
//      creditsTextField.layer.cornerRadius = 20
//      creditsTextField.layer.borderColor = UIColor.init(white: 1, alpha: 0.5).cgColor
//      creditsTextField.layer.borderWidth = 1
//      creditsTextField.layer.masksToBounds = true
//      self.scene?.view?.addSubview(creditsTextField)
//      
////      self.scene?.view?.addConstraint(NSLayoutConstraint(item: creditsTextField, attribute: .centerY, relatedBy: .equal, toItem: self.scene?.view!, attribute: .centerY, multiplier: 1, constant: 0))
////      self.scene?.view?.addConstraint(<#T##constraint: NSLayoutConstraint##NSLayoutConstraint#>)
//   }
   
   private func getKeyChainData() {
      if userData == nil {
         
         let retrievedString: String? = KeychainWrapper.standard.string(forKey: "highScore")
         if retrievedString == nil {
            let saveSuccessful: Bool = KeychainWrapper.standard.set("0", forKey: "highScore")
            if saveSuccessful {
               let highScoreString =  KeychainWrapper.standard.string(forKey: "highScore")
               
               if let highscore = Int(highScoreString!) {
                  currentUserData = CurrentUserData(highscore: highscore)
                  print("saving successful. highScore = \(String(describing: currentUserData!.highscore))")
               }
            }
         } else {
            print("Keychain has data.\n\thighScore = \(retrievedString!)")
            if let highscore = Int(retrievedString!) {
               currentUserData = CurrentUserData(highscore: highscore)
               print("saved to currentUserData.\n\thighScore = \(String(describing: currentUserData!.highscore))")
            }
//            let removeSuccessful: Bool = KeychainWrapper.standard.removeObject(forKey: "highScore")
//            if removeSuccessful {
//               let retrievedString: String? = KeychainWrapper.standard.string(forKey: "highScore")
//               if retrievedString == nil {
//                  print("highScore = nil")
//               } else {
//                  print(retrievedString!)
//               }
//            }
         }
         
      }
   }
   
   //ingameTimerToTap = Timer.scheduledTimer(timeInterval: 2.3, target: self, selector: #selector(notifyToTap), userInfo: nil, repeats: false)
   @objc private func notifyToTap() {
      notifyToTapNode.alpha = 1
      notifyToTapNode.run(.repeatForever(.sequence([
         .scale(to: 1.3, duration: 0.1),
         .scale(to: 1, duration: 0.1)
         ])))
   }
   
   private func disableNotifyToTap() {
      notifyToTapNode.removeAllActions()
      notifyToTapNode.setScale(1)
      notifyToTapNode.alpha = 0
   }
   
   override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
      let touch = touches.first!
      let touchLocation = touch.location(in: self)
      
      
      
      if allowInput {
         if !onSettings {
            if inGame == false {
               if settingsNode.contains(touchLocation) {
                  return
               } else {
                  startGame()
               }
            } else if pausedGame == false {
               if pauseNode.contains(touchLocation) {
                  return
               } else { // in-game
                  //            bubbleAnimation(atPoint: touchLocation)
                  //            cameraNode.run(.sequence([
                  //               .scale(to: 0.87, duration: 0.03),
                  //               .scale(to: 0.9, duration: 0.03)
                  //               ]))
                  disableNotifyToTap()
                  ingameTimerToTap.invalidate()
                  ingameTimerToTap = Timer.scheduledTimer(timeInterval: 1.7, target: self, selector: #selector(notifyToTap), userInfo: nil, repeats: false)
                  
                  cameraNode.run(.scale(to: 0.88, duration: 0.04))
                  deductGamePercentage()
                  //            print("screen tapped")
               }
            }
         }
      }
   }
   
   // Put here user defaults for music and effects settings.
   override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
      let defaults = UserDefaults.standard
      
      let touch = touches.first!
      let touchLocation = touch.location(in: self)
      if allowInput {
         if !onSettings {
            if inGame == true {
               // pausing the game
               if pauseNode.contains(touchLocation) {
                  didPauseGame(withPauseStatus: pausedGame)
               }
               if !pausedGame {
                  cameraNode.run(.scale(to: 0.9, duration: 0.04))
               }
            } else {
               if settingsNode.contains(touchLocation) {
                  goToView(.Settings)
//                  timer = Timer.scheduledTimer(timeInterval: 0.15, target: self, selector: #selector(addCreditsTextField), userInfo: nil, repeats: false)
                  print("settings touched")
               }
            }
            if pausedGame == true {
               // exiting the game
               if exitNode.contains(touchLocation) {
                  print("game exited")
                  checkScoreIfHighScoreBeatAndSaveToKeyChain()
                  startAnimation(phase: .GameOver)
               }
            }
         } else { // view is in settings
            if settingsNode.contains(touchLocation) {
               goToView(.Main)
//               creditsTextField.removeFromSuperview()
               print("back touched")
            }
            if musicCheckboxNode.contains(touchLocation) {
               musicOn = !musicOn
               
               defaults.set(musicOn, forKey: "toggleMusic")
               
               if musicCheckNode.alpha == 1 {
                  GameViewController.playSoundBG(false)
                  musicCheckNode.run(.fadeOut(withDuration: 0.1))
               } else {
                musicCheckNode.run(.fadeIn(withDuration: 0.1))
                  GameViewController.playSoundBG(true)
               }
            }
            if effectsCheckboxNode.contains(touchLocation) {
               effectsOn = !effectsOn
               
               defaults.set(effectsOn, forKey: "toggleFX")
               
               if effectsCheckNode.alpha == 1 {
                  effectsCheckNode.run(.fadeOut(withDuration: 0.1))
               } else {
                  effectsCheckNode.run(.fadeIn(withDuration: 0.1))
               }
            }
         }
      }
   }
   
   override func update(_ currentTime: TimeInterval) {
      if inGame && !isPaused && gameObject.score != gameObject.prevScore {
         scoreLabelNode.text = String(gameObject.score)
         scoreLabelNode.run(.sequence([ .scale(to: 1.5, duration: 0.1), .scale(to: 1, duration: 0.1) ]))
         gameObject.prevScore = gameObject.score
      }
   }
   
   private func addGameObjectScore() {
      gameObject.score += 1
   }
   
   
   //MARK: - NODE SETTINGS AND ANIMATIONS
   enum AppPhase {
      case Entry
      case NewGame
      case GameOver
//      case EndGame
   }
   private func startAnimation(phase: AppPhase) {
      switch phase {
      case .Entry:
         setTimerBeforeAllowingInput(withTimeInterval: 1.5)
         //settingsView node settings
         setAndAnimateSettingsNodes()
         
         //node settings
         exitLabelNode.alpha = 0
         dimNode.alpha = 0
         pauseIconNode.alpha = 0
         logoNode.alpha = 0
            //score nodes
         showInGameScore(false)
         
         changeMeterComponentsAlpha(value: 0)
         animateLogoSpring()
         setBobToDefault()
         meterPercentageNode.size.height = MeterPercentage.Height.Max.rawValue
//         meterVibrancyNode.size.height = MeterVibrancy.Height.Min.rawValue
                  inGame = false
         //animations
         settingsNode.run(.sequence([.fadeOut(withDuration: 0.0),
                                     .wait(forDuration: 0.6),
                                     .fadeIn(withDuration: 0.5)]))
         tapToPlayLabelNode.run(.repeatForever(.sequence([.fadeOut(withDuration: 1), .fadeIn(withDuration: 1)])))
         
         //main animation
         let (node1, node2) = animateBobMainMenu()
         headNode.texture = SKTexture(imageNamed: "BobHeadWasted")
         //save Animated Nodes
         animatedNodes.append(node1)
         animatedNodes.append(node2)
         
         
      case .NewGame:
         setTimerBeforeAllowingInput(withTimeInterval: 1.5)
         ingameTimerToTap = Timer.scheduledTimer(timeInterval: 2.3, target: self, selector: #selector(notifyToTap), userInfo: nil, repeats: false)
         print("settings touched")
         if !isRestarted {
            // from previous AppPhase
            tapToPlayLabelNode.removeAllActions()
            tapToPlayLabelNode.alpha = 0
            unPauseAllAnimatedNodes()
            removeAllActionsInAllAnimatedNodes()
         } else {
            settingsNode.alpha = 0
            setAndAnimateSettingsNodes()
            
            //node settings
            exitLabelNode.alpha = 0
            dimNode.alpha = 0
            pauseIconNode.alpha = 0
            logoNode.alpha = 0
            tapToPlayLabelNode.alpha = 0
            //score nodes
            setBobToDefault()
         }
         
         // initialize Game Class
         gameObject = NewGame()
         allowInput = false
         //node settings
         meterPercentageNode.size.height = MeterPercentage.Height.Max.rawValue
//         meterVibrancyNode.size.height = MeterVibrancy.Height.Min.rawValue
         showInGameScore(true)
         scoreLabelNode.text = "0"
         
         
         //animations
         logoNode.removeAllActions()
         logoNode.run(.sequence([.fadeOut(withDuration: 0.6), .moveBy(x: 0, y: -20, duration: 0)]))
         settingsNode.run(.fadeOut(withDuration: 0.6))
         cameraNode.run(.scale(to: 0.9, duration: 1))
//         pauseLabelNode.run(.sequence([.wait(forDuration: 1.5),.fadeAlpha(to: 0.3, duration: 1)]))
         pauseIconNode.run(.sequence([.wait(forDuration: 0.5),.fadeAlpha(to: 1, duration: 1)]))
         meterNode.run(.flashForNewGameAnimation)
         meterPercentageNode.run(.flashForNewGameAnimation)
         
//         meterVibrancyNode.run(.flashForNewGameAnimation)
         
         meterPercentageNode.run(.repeatForever(.sequence([
            .run(animateMeterPercentage),
            .wait(forDuration: 0.1)])))
//         meterPercentageNode.run(.repeatForever(.run(animateMeterPercentage)))
         
         //game animation
         let (node1, node2, node3) = animateBobInGame()
         headNode.texture = SKTexture(imageNamed: "BobHead")
         
         //save Animated Nodes
         animatedNodes.append(meterNode)
//         animatedNodes.append(meterVibrancyNode)
         animatedNodes.append(meterPercentageNode)
         animatedNodes.append(node1)
         animatedNodes.append(node2)
         animatedNodes.append(node3)
         
      case .GameOver:
//         allowInput = false
         if let scene = GameOverSceneClass(fileNamed: "GameOverScene") {
            let reveal = SKTransition.fade(withDuration: 1)
            scene.scaleMode = .aspectFill
            self.view?.presentScene(scene, transition:reveal)
            //            print("GameOverScene presented.")
         }
//         let nextScene = GameOverSceneClass(fileNamed: "GameOverScene")
//         let reveal = SKTransition.fade(withDuration: 1)
//         nextScene!.scaleMode = .aspectFill
//         self.view!.presentScene(nextScene!, transition:reveal)
         
//         if let nextScene = GameOverSceneClass(fileNamed: "GameOverScene") {
//            let reveal = SKTransition.fade(withDuration: 1)
//            nextScene.scaleMode = .aspectFill
//            self.view?.presentScene(nextScene, transition:reveal)
////            print("GameOverScene presented.")
//         } else {
//            print("Error: unable to load scene.")
//         }

         
         
            
//      case .EndGame:
//         setTimerBeforeAllowingInput(withTimeInterval: 1.5)
//         // from previous AppPhase
//         tapToPlayLabelNode.run(.repeatForever(.sequence([.fadeOut(withDuration: 1), .fadeIn(withDuration: 1.2)])))
//         unPauseAllAnimatedNodes()
//         removeAllActionsInAllAnimatedNodes()
//         
//         //node settings
//         setBobToDefault()
//         dimNode.alpha = 0
//         allowInput = false
////         pauseLabelNode.text = "pause"
////         pauseLabelNode.alpha = 0
//         pauseIconNode.alpha = 0
//         exitLabelNode.alpha = 0
//         logoNode.alpha = 0
//         changeMeterComponentsAlpha(value: 0)
//         //      resetLogoScale(withAnimation: true)
//         //animations
//         self.cameraNode.run(.scale(to: 1, duration: 0.2))
//         logoNode.run(.moveBy(x: 0, y: 20, duration: 0))
//         animateLogoSpring()
//         //      logoNode.run(.fadeIn(withDuration: 1))
//         settingsNode.run(.fadeIn(withDuration: 0.6))
//         //clear saved nodes and pauseGame to default
//         pausedGame = false
//         animatedNodes.removeAll()
//         inGame = false
//         gameObject = nil
//         
//         //main animation
//         let (node1, node2) = animateBobMainMenu()
//         headNode.texture = SKTexture(imageNamed: "BobHeadWasted")
//         //save Animated Nodes
//         animatedNodes.append(node1)
//         animatedNodes.append(node2)
      }
      
   }
   
   //  MARK: - FUNCTIONS
   private func showInGameScore(_ show: Bool) {
      if show {
         scoreLabelNode.run(.flashForNewGameAnimation)
         mugsNode.run(.flashForNewGameAnimation)
         scoreBackgroundNode.run(.fadeAlpha(to: 0.5, duration: 1.5))
//         scoreBackgroundNode.run(.flashForNewGameAnimation)
      } else {
         scoreLabelNode.alpha = 0
         mugsNode.alpha = 0
         scoreBackgroundNode.alpha = 0
      }
//      scoreLabelNode.alpha = show ? 1 : 0
//      mugsNode.alpha = show ? 1 : 0
//      scoreBackgroundNode.alpha = show ? 1 : 0
   }
   
   private func initializeSceneNodes() {
      self.headNode = self.childNode(withName: "//headNode") as? SKSpriteNode
      self.armNode = self.childNode(withName: "//armNode") as? SKSpriteNode
      self.mugNode = self.childNode(withName: "//armNode")?.childNode(withName: "//mugNode") as? SKSpriteNode
      self.logoNode = self.childNode(withName: "//logoNode") as? SKSpriteNode
      self.cameraNode = self.childNode(withName: "cameraNode") as? SKCameraNode
      self.meterNode = self.childNode(withName: "//meterNode") as? SKSpriteNode
      
      self.meterPercentageNode = self.childNode(withName: "meterPercentageNode") as? SKSpriteNode
//      self.meterVibrancyNode = self.childNode(withName: "meterVibrancyNode") as? SKSpriteNode
//      self.meterVibrancyNode = self.childNode(withName: "meterPercentageNode") as? SKSpriteNode
//      self.meterPercentageNode = self.childNode(withName: "meterVibrancyNode") as? SKSpriteNode
      
      self.settingsNode = self.childNode(withName: "settingsNode") as? SKSpriteNode
//      self.pauseLabelNode = self.childNode(withName: "pauseLabelNode") as? SKLabelNode
      self.pauseNode = self.childNode(withName: "pauseNode") as? SKSpriteNode
      self.exitLabelNode = self.childNode(withName: "exitLabelNode") as? SKLabelNode
      self.exitNode = self.childNode(withName: "exitNode") as? SKSpriteNode
      self.pauseIconNode = self.childNode(withName: "//pauseIconNode") as? SKSpriteNode
      
      self.tapToPlayLabelNode = self.childNode(withName: "//tapToPlayLabelNode") as? SKLabelNode
      self.notifyToTapNode = self.childNode(withName: "notifyToTapNode") as? SKLabelNode
      
      self.musicCheckboxNode = self.childNode(withName: "//musicCheckboxNode") as? SKSpriteNode
      self.effectsCheckboxNode = self.childNode(withName: "//effectsCheckboxNode") as? SKSpriteNode
      self.musicCheckNode = self.childNode(withName: "//musicCheckNode") as? SKSpriteNode
      self.effectsCheckNode = self.childNode(withName: "//effectsCheckNode") as? SKSpriteNode
      self.musicLabelNode = self.childNode(withName: "//musicLabelNode") as? SKLabelNode
      self.effectsLabelNode = self.childNode(withName: "//effectsLabelNode") as? SKLabelNode
      self.creditsNode = self.childNode(withName: "creditsNode")

      self.mugsNode = self.childNode(withName: "mugsNode") as? SKSpriteNode
      self.scoreLabelNode = self.childNode(withName: "scoreLabelNode") as? SKLabelNode
      self.scoreBackgroundNode = self.childNode(withName: "scoreBackgroundNode") as? SKSpriteNode
      
      
      
      let gushPath = Bundle.main.path(forResource: "beerGushEmitter",
                                                            ofType: "sks")
      let gushNode = NSKeyedUnarchiver.unarchiveObject(withFile: gushPath!)
         as! SKEmitterNode
      gushNode.position = CGPoint(x: 107.066, y: 376.746)
      gushNode.particleBirthRate = 0
      beerGushEmmiter = gushNode
      self.addChild(beerGushEmmiter)
      
//      self.beerGushEmmiter = SKEmitterNode(fileNamed: "beerGushEmitter.sks")
//      beerGushEmmiter.position = CGPoint(x: 107.066, y: 376.746)
//      beerGushEmmiter.particleBirthRate = 0 //62.78
      
      notifyToTapNode.alpha = 0
      creditsNode.alpha = 0
   }
   
   @objc private func changeAllowInput() {
      allowInput = !allowInput
   }
   
   private func setTimerBeforeAllowingInput(withTimeInterval timeInSeconds: TimeInterval) {
      timer = Timer.scheduledTimer(timeInterval: timeInSeconds, target: self, selector: #selector(changeAllowInput), userInfo: nil, repeats: false)
   }
   
   private func deductGamePercentage() {
      if gameObject.drunkPercentage != 0 {
         gameObject.drunkPercentage -= gameObject.deductedPercentage
         if gameObject.drunkPercentage <= 0 {
            gameObject.drunkPercentage = 0
         }
      }
   }
   
   private func addGamePercentage() {
      if inGame && !pausedGame {
         gameObject.drunkPercentage += gameObject.addedPercentage
         if gameObject.drunkPercentage >= 0 && gameObject.drunkPercentage <= 79 {
            changeHeadTexture(texture: .Default)
         } else if gameObject.drunkPercentage >= 80 && gameObject.drunkPercentage <= 94 {
            changeHeadTexture(texture: .Tipsy)
         } else if gameObject.drunkPercentage >= 95 {
            changeHeadTexture(texture: .Wasted)
         }
      }
   }
   
   enum BobHeadTexture: String {
      case Default = "BobHead"
      case Tipsy = "BobHeadTipsy"
      case Wasted = "BobHeadWasted"
   }
   private func changeHeadTexture(texture: BobHeadTexture) {
      headNode.texture = SKTexture(imageNamed: texture.rawValue)
   }
   
   private enum View {
      case Main
      case Settings
   }
   private func goToView(_ view: View) {
      switch view {
      case .Main:
         onSettings = false
         setAndAnimateSettingsNodes(startGame: false, showNodes: false)
         cameraNode.run(.moveBy(x: -240, y: 0, duration: 0.2))
         settingsNode.run(.sequence([.fadeOut(withDuration: 0), .fadeIn(withDuration: 0.5)]))
         settingsNode.texture = SKTexture(imageNamed: "settingsIcon")
      case .Settings:
         onSettings = true
         setAndAnimateSettingsNodes(startGame: false, showNodes: true)
         cameraNode.run(.moveBy(x: 240, y: 0, duration: 0.2))
         settingsNode.run(.sequence([.fadeOut(withDuration: 0), .fadeIn(withDuration: 0.5)]))
         settingsNode.texture = SKTexture(imageNamed: "quitIcon")
      }
   }
   
   private func setAndAnimateSettingsNodes(startGame: Bool = true, showNodes show:Bool = true) {
      if startGame {
         musicCheckboxNode.alpha = 0
         effectsCheckboxNode.alpha = 0
         musicCheckNode.alpha = 0
         effectsCheckNode.alpha = 0
         musicLabelNode.alpha = 0
         effectsLabelNode.alpha = 0
         creditsNode.alpha = 0
      } else {
         if show {
            musicCheckboxNode.run(.fadeIn(withDuration: 0.5))
            effectsCheckboxNode.run(.fadeIn(withDuration: 0.5))
            musicLabelNode.run(.fadeIn(withDuration: 0.5))
            effectsLabelNode.run(.fadeIn(withDuration: 0.5))
            creditsNode.run(.fadeIn(withDuration: 0.5))
            
            if musicOn {
               musicCheckNode.run(.fadeIn(withDuration: 0.5))
            }
            if effectsOn {
               effectsCheckNode.run(.fadeIn(withDuration: 0.5))
            }
            
            dimScreen(true)
            
         } else {
            musicCheckboxNode.run(.fadeOut(withDuration: 0.2))
            effectsCheckboxNode.run(.fadeOut(withDuration: 0.2))
            musicCheckNode.run(.fadeOut(withDuration: 0.2))
            effectsCheckNode.run(.fadeOut(withDuration: 0.2))
            musicLabelNode.run(.fadeOut(withDuration: 0.2))
            effectsLabelNode.run(.fadeOut(withDuration: 0.2))
            creditsNode.run(.fadeOut(withDuration: 0.2))
            
            dimScreen(false)
         }
      }
   }

   @objc private func checkIfEndGame() {
      print(meterPercentageNode.size.height)
      if meterPercentageNode.size.height <= 0.1 {
         //keychain saving and screenshot
         print("game ended")
         checkScoreIfHighScoreBeatAndSaveToKeyChain()
         startAnimation(phase: .GameOver)
      }
   }
   
   private func saveScreenshotToGameObject() {
      UIGraphicsBeginImageContextWithOptions(UIScreen.main.bounds.size, false, 0)
      self.view?.drawHierarchy(in: (view?.bounds)!, afterScreenUpdates: true)
      let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
      UIGraphicsEndImageContext()
      gameObject.screenshot = image
   }
   
   private func checkScoreIfHighScoreBeatAndSaveToKeyChain() {
      let retrievedString: String? = KeychainWrapper.standard.string(forKey: "highScore")
      if retrievedString != nil {
         
         if currentUserData!.highscore < gameObject.score {
            //screenshot
            saveScreenshotToGameObject()
            //highscore saving
            let scoreString: String = String(gameObject.score)
            let saveSuccessful: Bool = KeychainWrapper.standard.set(scoreString, forKey: "highScore")
            if saveSuccessful {
               let highScoreString =  KeychainWrapper.standard.string(forKey: "highScore")
               
               if let highscore = Int(highScoreString!) {
                  currentUserData?.highscore = highscore
                  print("Highscore changed.\n\thighScore = \(String(describing: currentUserData!.highscore))")
               }
            }
         } else {
            print("Highscore not changed. Did not attain highscore.")
         }
      }
   }
   
   /// Game class initialization and initial animation is run.
   private func startGame() {
      if !inGame {
         print("game started")
         inGame = true
         startAnimation(phase: .NewGame)
         taps = 0
      }
   }
   
   private func initializeGameComponents() {
      let dimPanel = SKSpriteNode(color: UIColor.black, size: CGSize.init(width: 800, height: self.frame.height))
      dimPanel.alpha = 0
      dimPanel.zPosition = 8
      dimPanel.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
      self.addChild(dimPanel)
      self.dimNode = dimPanel
      
      initializeSceneNodes()
   }
   
   private func animateBobMainMenu() -> (SKSpriteNode, SKSpriteNode) {
      //total 2.8 sec
      //wait 3 sec
      //head
      headNode.run(SKAction.rotate(toAngle: Bob.Head.Up.rawValue, duration: 0))
      
      let headDown = SKAction.rotate(toAngle: Bob.Head.Default.rawValue, duration: 1)
      let headUp = SKAction.rotate(toAngle: Bob.Head.Up.rawValue, duration: 0.4)
//      let headDown = SKAction.rotate(toAngle: Bob.Head.Down.rawValue, duration: 0.2)
//      let headUp = SKAction.rotate(toAngle: Bob.Head.Default.rawValue, duration: 1.5)
      headDown.timingMode = .easeInEaseOut
      headUp.timingMode = .easeInEaseOut
      headNode.run(.repeatForever(.sequence([
         headDown,
         headUp,
//         .wait(forDuration: 0.1),
//         .rotate(byAngle: 0.0698131700797732, duration: 0.4),
         .rotate(byAngle: -0.0698131700797732, duration: 0.4),
         .rotate(byAngle: 0.0698131700797732, duration: 0.4),
         .rotate(byAngle: -0.0698131700797732, duration: 0.4),
         .rotate(byAngle: 0.0698131700797732, duration: 0.4),
         .rotate(byAngle: -0.0698131700797732, duration: 0.4),
         .rotate(byAngle: 0.0698131700797732, duration: 0.4),
         .rotate(byAngle: -0.0698131700797732, duration: 0.4),
         .rotate(byAngle: 0.0698131700797732, duration: 0.4),
         .rotate(byAngle: -0.0698131700797732, duration: 0.4),
         .rotate(byAngle: 0.0698131700797732, duration: 0.4),
         .rotate(byAngle: -0.0698131700797732, duration: 0.4),
         .rotate(byAngle: 0.0698131700797732, duration: 0.4),
         .rotate(byAngle: -0.0698131700797732, duration: 0.4),
         .rotate(byAngle: 0.0698131700797732, duration: 0.4),
         .rotate(byAngle: -0.0698131700797732, duration: 0.4)
//         .wait(forDuration: 5)
//         headUp,
//         headDown,
//         .wait(forDuration: 5)
         ])))
      
      
      //arm
      let armUp = SKAction.rotate(toAngle: Bob.Arm.PartialRaised.rawValue, duration: 1.7)
      let armDown = SKAction.rotate(toAngle: Bob.Arm.Default.rawValue, duration: 0.2)
      armUp.timingMode = .easeInEaseOut
      armDown.timingMode = .easeIn
      armNode.run(.repeatForever(.sequence([
         .wait(forDuration: 2),
         armUp,
         armDown,
         .wait(forDuration: 3.5)
         ])))
      
      return (headNode, armNode)
   }
   
   @objc private func deductPointFivePercent() {
      gameObject.deductedPercentage = gameObject.deductedPercentage + 0.5
   }
   
   func generateRandomNumber(min: Double, max: Double) -> Double {
      let randomNum = Double(arc4random_uniform(UInt32(max) - UInt32(min)) + UInt32(min))
      return randomNum
   }
   
   private func increaseDifficulty() {
      if (gameObject.score > 0 && gameObject.score % 2 == 0 && gameObject.deductedPercentage > 0.2) {
         gameObject.deductedPercentage = gameObject.deductedPercentage - 0.1
         
         if gameObject.deductedPercentage >= 0.5 {
            if timerEaseDifficulty != nil {
               timerEaseDifficulty?.invalidate()
               timerEaseDifficulty = nil
            }
         } else {
            if timerEaseDifficulty != nil {
               timerEaseDifficultyToInvalidate = TimeInterval(generateRandomNumber(min:3 , max: 5))
               Timer.scheduledTimer(timeInterval: timerEaseDifficultyToInvalidate!, target: self, selector: #selector(deductPointFivePercent), userInfo: nil, repeats: false)
            }
         }
         print(gameObject.deductedPercentage)
      }
   }
   
   private func playSoundSlide() {
      if effectsOn {
         GameViewController.loadFXFile(ofName: "slide", soundFileExtension: "mp3", forSoundType: .slideFX)
      }
   }
   
   private func playSoundGulp() {
      
   }
   
   @objc private func emitBeer() {
      beerGushEmmiter.particleBirthRate = 62.78
      Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(disableEmitBeer), userInfo: nil, repeats: false)
   }
   
   @objc private func disableEmitBeer() {
      beerGushEmmiter.particleBirthRate = 0
   }
   
   private func animateBobInGame() -> (SKSpriteNode, SKSpriteNode, SKSpriteNode) {
      //total 2.8 sec
//      if timerEmitter == nil {
//         timerEmitter = Timer.scheduledTimer(timeInterval: 2.8, target: self, selector: #selector(activateEmitter), userInfo: nil, repeats: true)
//      }
      //head
      let headUp = SKAction.rotate(toAngle: Bob.Head.Up.rawValue, duration: 0.35)
      let headDown = SKAction.rotate(toAngle: Bob.Head.Default.rawValue, duration: 0.6)
      headUp.timingMode = .easeInEaseOut
      headDown.timingMode = .easeInEaseOut
      headNode.run(.repeatForever(.sequence([
         .wait(forDuration: 0.6), headUp,
         .wait(forDuration: 0.2), headDown,
         .wait(forDuration: 1.05)
         ])))
      
      //arm
      let armUp = SKAction.rotate(toAngle: Bob.Arm.Raised.rawValue, duration: 0.35)
      let armDown = SKAction.rotate(toAngle: Bob.Arm.Default.rawValue, duration: 0.7)
      armUp.timingMode = .easeInEaseOut
      armDown.timingMode = .easeInEaseOut
      
      armNode.run(.repeatForever(.sequence([
         .rotate(toAngle: Bob.Arm.PartialRaised.rawValue, duration: 0.6),
         armUp,
         .run(addGamePercentage),
         .run(increaseDifficulty),//calls functions
         .run(emitBeer),
         .wait(forDuration: 0.2),
         .run(addGameObjectScore),
         armDown,
         .wait(forDuration: 0.95),
         ])))
      
      let moveToView = SKAction.move(to: CGPoint(x: BeerPosition.OnHand.X.rawValue, y: BeerPosition.OnHand.Y.rawValue), duration: 0.8)
      moveToView.timingMode = .easeInEaseOut
      
      //mug
      mugNode.run(.repeatForever(.sequence([
         .wait(forDuration: 0.95),
         .fadeOut(withDuration: 0.2),
         .moveTo(x: BeerPosition.OutOfScreen.X.rawValue, duration: 0.0),
         .moveTo(y: BeerPosition.OutOfScreen.Y.rawValue, duration: 0.0),
         .wait(forDuration: 0.5),
         .fadeIn(withDuration: 0.0),
         .run(playSoundSlide),
         moveToView,
         .wait(forDuration: 0.35)
         ])))
      
      return (armNode, headNode, mugNode)
   }

   private func didPauseGame(withPauseStatus pause: Bool) {
      if !pause {
         notifyToTapNode.alpha = 0
         ingameTimerToTap.invalidate()
         
         pauseIconNode.alpha = 0.5
         exitLabelNode.alpha = 1
//         logoNode.alpha = 1
         animateLogoSpring()
         dimScreen(true)
         pausedGame = !pausedGame
         pauseAllAnimatedNodes()
         print("game paused")
      } else {
         notifyToTapNode.alpha = 0
         ingameTimerToTap = Timer.scheduledTimer(timeInterval: 1.7, target: self, selector: #selector(notifyToTap), userInfo: nil, repeats: false)
         
         logoNode.removeAllActions()
         pauseIconNode.alpha = 1
         
         exitLabelNode.alpha = 0
         logoNode.alpha = 0
         logoNode.removeAllActions()
         dimScreen(false)
         pausedGame = !pausedGame
         unPauseAllAnimatedNodes()
         print("game unpaused")
      }
   }
   
   private func dimScreen(_ dim: Bool) {
      if dim {
         dimNode.alpha = 0.3
      } else {
         dimNode.alpha = 0.0
      }
   }
   
   
   //MARK: - ANIMATIONS
   private func animateLogoSpring() {
      let myAction = SKAction.init(named: "spring")!
      logoNode.run(.repeatForever(myAction))
   }

   private func animateMeterPercentage() {
      var percentage = gameObject.drunkPercentage
      if percentage >= 100 {
         percentage = 100.0
      }
      let height = CGFloat(((100.0 - percentage) * Double(MeterPercentage.Height.Max.rawValue))/100)
//      meterPercentageNode.run(.resize(toHeight: CGFloat(height), duration: 0.3))
      meterPercentageNode.run(.sequence([.resize(toHeight: height, duration: 0.3), .wait(forDuration: 0.1), .run(checkIfEndGame)]))
   }
   
   
   //  MARK: - ANIMATEDNODES FUNCTIONS
   private func removeAllActionsInAllAnimatedNodes() {
      for eachNode in animatedNodes {
         eachNode.removeAllActions()
      }
   }
   
   private func pauseAllAnimatedNodes() {
      for eachNode in animatedNodes {
         eachNode.isPaused = true
      }
   }
   
   private func unPauseAllAnimatedNodes() {
      for eachNode in animatedNodes {
         eachNode.isPaused = false
      }
   }
   
//   private func increaseSpeedAnimatedNodes() {
//      for eachNode in animatedNodes {
//         eachNode.speed = eachNode.speed + 0.2
//      }
//   }
   
   //MARK: - NODE SETTINGS
   private func changeMeterComponentsAlpha(value: CGFloat) {
      meterNode.alpha = value
//      meterVibrancyNode.alpha = value
      meterPercentageNode.alpha = value
   }
   
   private func setBobToDefault() {
      if  headNode.zRotation != Bob.Head.Default.rawValue {
         headNode.zRotation = Bob.Head.Default.rawValue
         headNode.texture = SKTexture(imageNamed: "BobHead")
      }
      if armNode.zRotation != Bob.Arm.Default.rawValue {
         armNode.zRotation = Bob.Arm.Default.rawValue
      }
      if mugNode.zRotation != Bob.Mug.Default.rawValue {
         mugNode.zRotation = Bob.Mug.Default.rawValue
         mugNode.alpha = 1
         mugNode.position = CGPoint(x: BeerPosition.OnHand.X.rawValue, y: BeerPosition.OnHand.Y.rawValue)
      }
   }
}

extension SKAction {
   static let flash = SKAction.sequence([
      .fadeAlpha(to: 0, duration: 0.3),
      .fadeAlpha(to: 1, duration: 0.3),
      .fadeAlpha(to: 0, duration: 0.3),
      .fadeAlpha(to: 1, duration: 0.3),
      .fadeAlpha(to: 0, duration: 0.3),
      .fadeAlpha(to: 1, duration: 0.3)
      ])
   static let flashForNewGameAnimation = SKAction.sequence([
      .fadeAlpha(to: 0.5, duration: 0.5),
      .fadeAlpha(to: 0, duration: 0.4),
      .fadeAlpha(to: 0.8, duration: 0.3),
      .fadeAlpha(to: 0, duration: 0.2),
      .fadeAlpha(to: 1, duration: 0.2)
      ])
}

extension UserDefaults {
   
   func hasValue(forKey key: String) -> Bool {
      return nil != object(forKey: key)
   }
}











































//MARK: - LATAK

//    bubbleNode?.physicsBody = SKPhysicsBody(circleOfRadius: self.size.width + self.size.height / 2)
//    bubbleNode?.physicsBody?.collisionBitMask = 0
//    meterNode.run(Animation.NewGame.MeterNode.animate())

//        // Get label node from scene and store it for use later
//        self.label = self.childNode(withName: "//helloLabel") as? SKLabelNode
//        if let label = self.label {
//            label.alpha = 0.0
//            label.run(SKAction.fadeIn(withDuration: 2.0))
//        }
//
//        // Create shape node to use during mouse interaction
//        let w = (self.size.width + self.size.height) * 0.05
//        self.spinnyNode = SKShapeNode.init(rectOf: CGSize.init(width: w, height: w), cornerRadius: w * 0.3)
//
//        if let spinnyNode = self.spinnyNode {
//            spinnyNode.lineWidth = 2.5
//
//            spinnyNode.run(SKAction.repeatForever(SKAction.rotate(byAngle: CGFloat(Double.pi), duration: 1)))
//            spinnyNode.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
//                                              SKAction.fadeOut(withDuration: 0.5),
//                                              SKAction.removeFromParent()]))
//        }


//    func touchDown(atPoint pos : CGPoint) {
//        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
//            n.position = pos
//            n.strokeColor = SKColor.green
//            self.addChild(n)
//        }
//    }
//
//    func touchMoved(toPoint pos : CGPoint) {
//        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
//            n.position = pos
//            n.strokeColor = SKColor.blue
//            self.addChild(n)
//        }
//    }
//
//    func touchUp(atPoint pos : CGPoint) {
//        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
//            n.position = pos
//            n.strokeColor = SKColor.red
//            self.addChild(n)
//        }
//    }
//
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        if let label = self.label {
//            label.run(SKAction.init(named: "Pulse")!, withKey: "fadeInOut")
//        }
//
//        for t in touches { self.touchDown(atPoint: t.location(in: self)) }
//    }
//
//    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
//        for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
//    }
//
//    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
//    }
//
//    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
//        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
//    }
//
//
//    override func update(_ currentTime: TimeInterval) {
//        // Called before each frame is rendered
//    }






































