//
//  LeaderboardsTableView.swift
//  BeerBob
//
//  Created by Anderson Pagtabunan on 31/05/2017.
//  Copyright © 2017 Anderson Pagtabunan. All rights reserved.
//

import UIKit

class LeaderboardsTableView: UITableView,UITableViewDelegate,UITableViewDataSource {
   var player: [String] = ["Player 1", "Player 2", "Player 3", "Player 4", "Player 5", "Player 6", "Player 7", "Player 8"]
   var score: [String] = ["100,000", "100,000,000", "1,000", "100", "100,000", "100,000,000", "1,000", "100"]
   
   private var cellId = "cellId"
   
   override init(frame: CGRect, style: UITableViewStyle) {
      super.init(frame: frame, style: style)
      self.delegate = self
      self.dataSource = self
      self.rowHeight = UITableViewAutomaticDimension + 40
      self.estimatedRowHeight = 44
      self.backgroundColor = UIColor.clear
      self.separatorStyle = .none
      
      self.register(LeaderboardsTableViewCell.self, forCellReuseIdentifier: cellId)
   }
   
   required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   // MARK: - Table view data source
   func numberOfSections(in tableView: UITableView) -> Int {
      return 1
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return self.player.count
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
      let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! LeaderboardsTableViewCell
      let playerNameLabel = player[indexPath.row]
      let scoreLabel = score[indexPath.row]
      
      cell.playerNameLabel.text = playerNameLabel
      cell.scoreLabel.text = scoreLabel
      
      cell.isUserInteractionEnabled = false
      return cell
   }
   
   
//   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//      print("You selected cell #\(indexPath.row)!")
//   }
}

class LeaderboardsTableViewCell: UITableViewCell {
   
   static let labelSize: CGFloat = 13
   
   let playerNameLabel: UILabel = {
      
      let label = UILabel()
      label.font = UIFont.systemFont(ofSize: labelSize)
      label.textColor = UIColor.white
      label.textAlignment = .left
      label.text = "Player"
      
      return label
   }()
   
   let scoreLabel: UILabel = {
      
      let label = UILabel()
      label.font = UIFont.systemFont(ofSize: labelSize)
      label.textColor = UIColor.white
      label.textAlignment = .right
      label.text = "100,000"
      
      return label
   }()
   
   override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      
      backgroundColor = UIColor.clear
      //      alpha = 0.5
      contentView.isUserInteractionEnabled = false
      //       Initialization code
      contentView.addSubview(playerNameLabel)
      contentView.addSubview(scoreLabel)
      
      playerNameLabel.translatesAutoresizingMaskIntoConstraints = false
      scoreLabel.translatesAutoresizingMaskIntoConstraints = false
      
      addConstraintsWithFormat("H:|-5-[v0][v1]-5-|", views: playerNameLabel,scoreLabel)
      addConstraintsWithFormat("V:|[v0]|", views: playerNameLabel)
      addConstraintsWithFormat("V:|[v0]|", views: scoreLabel)
   }
   
   required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   func addConstraintsWithFormat(_ format: String, views: UIView...) {
      
      var viewsDictionary = [String: UIView]()
      for (index, view) in views.enumerated() {
         let key = "v\(index)"
         viewsDictionary[key] = view
         view.translatesAutoresizingMaskIntoConstraints = false
      }
      
      contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(),
                                                                metrics: nil, views: viewsDictionary))
   }
}

