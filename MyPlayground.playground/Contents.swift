//: Playground - noun: a place where people can play

import UIKit
import SpriteKit

postfix operator °
postfix func ° (degrees: CGFloat) -> CGFloat {
  return degrees * CGFloat(.pi / 180.0)
}

func DegreesOfRadians( radians: CGFloat) -> CGFloat {
  return radians / CGFloat(.pi / 180.0)
}


let myDegrees: CGFloat = 7.952
print((myDegrees)°)


let myRadians: CGFloat = 0.0917868653623818
print(DegreesOfRadians(radians: myRadians))

//var myNode: SKSpriteNode = SKSpriteNode()
//var myNode1: SKSpriteNode = SKSpriteNode()
//var myNode2: SKSpriteNode = SKSpriteNode()
//var activeNodes: [SKSpriteNode] = []
//
//func appendToActiveNodes(initializedNodeName node: SKSpriteNode) {
//  activeNodes.append(node)
//}
//
//appendToActiveNodes(initializedNodeName: myNode)
//appendToActiveNodes(initializedNodeName: myNode1)
//appendToActiveNodes(initializedNodeName: myNode2)
//
//print(activeNodes.count)
